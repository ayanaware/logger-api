'use strict';

let Logger = null;
let LogLevel = null;
try {
	const ayanawareLogger = require('@ayanaware/logger');
	Logger = ayanawareLogger.Logger;
	LogLevel = ayanawareLogger.LogLevel;

	if (Logger == null || typeof Logger.getVersionMajor !== 'function') {
		console.warn('@ayanaware/logger-api: An unsupported version of @ayanaware/logger was found! Please update your @ayanaware/logger package.');
		throw 0;
	}

	const major = Logger.getVersionMajor();	
	const pkg = require('./package.json');
	const apiMajor = Number(pkg.version.split('.')[0]);

	if (apiMajor > major) {
		console.warn(`@ayanaware/logger-api: API with major version ${apiMajor} was loaded in a project that uses @ayanaware/logger with major version ${major}! Please update your @ayanaware/logger package.`);
		throw 0;
	} else if (major < apiMajor) {
		// TODO If v3 or any version after this changes API, the backwards compatible classes will be loaded here
	}
} catch (e) {
	Logger = class NoOpLogger {
		error() {}
		warn() {}
		info() {}
		debug() {}
		trace() {}
		log() {}
		internal() {}
		static get() { return new NoOpLogger() }
	};
	LogLevel = {
		OFF: 'OFF',
		ERROR: 'ERROR',
		WARN: 'WARN',
		INFO: 'INFO',
		DEBUG: 'DEBUG',
		TRACE: 'TRACE',
	};
}

module.exports.Logger = Logger;
module.exports.default = Logger;
module.exports.LogLevel = LogLevel;
